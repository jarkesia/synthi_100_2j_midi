/*
    Firmware for custom EMS MIDI converter for Synthi 100.
    Copyright (C) 2018  Jari Suominen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*              
         ,,'6''-,.
        <====,.;;--.
        _`---===. """==__
      //""@@-\===\@@@@ ""\\
     |( @@@  |===|  @@@  ||
      \\ @@   |===|  @@  //
        \\ @@ |===|@@@ //
         \\  |===|  //
___________\\|===| //_____,----""""""""""-----,_
  """"---,__`\===`/ _________,---------,____    `,
             |==||                           `\   \
            |==| |                             )   |
           |==| |       _____         ______,--'   '
           |=|  `----"""     `""""""""         _,-'
            `=\     __,---"""-------------"""''
                """"
 */

/*

  Custom EMS Synthi 100 MIDI controller replaces original keyboard input
  with MIDI input. It integrates seamlessly to the keyboard panel interface.

  Upper and lower keyboard have their individual MIDI channels that
  are set using dip switches on the controller PCB, upper switch
  for upper keyboard, lower for lower. Both DIP switch units have
  four individual switches, one for every bit. Binary coding is used
  for setting incoming MIDI channel from 0-15.

  For example (1=on, 0=down/unmarked):
  MIDI channel  0 = 0000
  MIDI channel  1 = 1000
  MIDI channel  2 = 0100
  MIDI channel 15 = 1111

  Your MIDI device might number MIDI channels starting from 1.
  This being the case, add 1 to the DIP MIDI channel number (0 is actually 1 etc).

  When using MIDI keyboard ( note on and note off commands only)
  to control the module, it should behave exactly how stock dual keyboard
  of Synthi 100 would, so when in doubt, study original documentation (3.7.15-1).

  The range of the keyboard are notes from b0 to d6, notes outside this range
  are ignored. There is not a single note that would output 0V CV, notes closest
  to it are f#3 and g3. When Synthi is switched on, pitch CVs are set to g3,
  which is also most useful CV value used for tuning.

  You can also manipulate the CV outputs using CC commands
  from 20-25 sent to one of the active MIDI channels.

  CC20: Upper keyboard pitch    (7-bits 0-127)
  CC21: Upper keyboard velocity (7-bits 0-127)
  CC22: Upper keyboard envelope (1-bit  0-63=off, 64-127=on)
  CC23: Lower keyboard pitch    (7-bits 0-127)
  CC24: Lower keyboard velocity (7-bits 0-127)
  CC25: Lower keyboard envelope (1-bit  0-63=off, 64-127=on)

  MIDI out connector is only used to send through all MIDI data sent in.

  On Radio Belgrade Synthi 100, MIDI is hardwired to socket OPTION 2.
  Studio should have special cable connecting to this socket, having
  two MIDI DIN connectors at the other end. The cable labeled with
  BLUE cable tie is the MIDI IN connector.

*/

/*
    FUSES: HF: 0xDE LF: 0xFF EF: 0xFD
    avrdude -c avrispmkII -p m644p -P /dev/ttyACM0 -U lfuse:w:0xff:m -U hfuse:w:0xde:m -U efuse:w:0xfd:m -F
*/

#include <MIDI.h>

MIDI_CREATE_DEFAULT_INSTANCE();

boolean notes[2][64];

/* Interface connections */
/* Upper Keyboard = [0], Lower Keyboard = [1] */
byte midiInChannel[2] = {1, 2};
volatile byte envelopeCV[2] = {11, 19}; // Pins
volatile byte newPitchSwitch[2] = {10, 16}; // Pins
volatile uint8_t *pitchCV[2];
volatile uint8_t *velocityCV[2];

#define RETRIGGER_PULSE_LENGTH 20 /* According to Paul's manual. */
boolean retriggerRequested[2] = {false, false};
long retriggerTimer[2] = {0, 0};

#define MIDI_KB_OFFSET 23
#define MIDI_KB_KEYS 63
#define SEVEN_BIT_MAX 254

#define NO_KEY 255
byte lastKey[2] = {NO_KEY, NO_KEY};


void setup() {

  /* Fast-PWM on every pin, pwm frequency ~62500Hz */
  TCCR1A = B10100001;
  TCCR1B = B00000001;
  TCCR2A = B10100001;
  TCCR2B = B00000001;

  /* Port A */
  /* PA0-3 Upper Keyboard MIDI channel DIP switch in */
  DDRA  = B00000000;
  PORTA = B00001111;

  /* Port B */
  /* PB0-3 Lower Keyboard MIDI channel DIP switch in */
  DDRB  = B00000000;
  PORTB = B00001111;

  /* Port C */
  /* PC0 Lower Keyboard New Pitch Switch in */
  /* PC3 Lower Keyboard Envelope out */
  DDRC  = B00001000;
  PORTC = B00000001;

  /* Port D */
  /* PD0 MIDI in RX */
  /* PD1 MIDI out TX */
  /* PD2 Upper Keyboard New Pitch Switch in */
  /* PD3 Upper Keyboard Envelope out */
  /* PD4 Upper Keyboard Velocity out PWM */
  /* PD5 Upper Keyboard Pitch out PWM */
  /* PD6 Lower Keyboard Velocity out PWM */
  /* PD7 Lower Keyboard Pitch out PWM */
  DDRD  = B11111010;
  PORTD = B00000100;

  /* PWM pins for CV out */
  pitchCV[0] = (uint8_t *)&OCR1A;
  pitchCV[1] = (uint8_t *)&OCR2A;
  velocityCV[0] = (uint8_t *)&OCR1B;
  velocityCV[1] = (uint8_t *)&OCR2B;

  MIDI.begin(MIDI_CHANNEL_OMNI);
  MIDI.setHandleNoteOn(midiNoteOn);
  MIDI.setHandleNoteOff(midiNoteOn);
  MIDI.setHandleControlChange(midiCC);
  MIDI.setHandleSystemReset(midiReset);
  //MIDI.turnThruOff();

  midiReset();

}


void loop() {

  while ( MIDI.read() ) {}

  for (int keyboard = 0; keyboard < 2; keyboard++) {
    if (retriggerRequested[keyboard] && ((millis() - retriggerTimer[keyboard]) > RETRIGGER_PULSE_LENGTH) ) {
      digitalWrite(envelopeCV[keyboard], HIGH);
      retriggerRequested[keyboard] = false;
    }
  }

  midiInChannel[0] = ((~PINA) & B00001111) + 1;
  midiInChannel[1] = ((~PINB) & B00001111) + 1;

}


void updateMIDIPitch(int keyboard) {
  for (int n = MIDI_KB_KEYS; n > -1; n--) {
    if (notes[keyboard][n]) {
      *pitchCV[keyboard] = SEVEN_BIT_MAX - n * 4;
      if (lastKey[keyboard] == NO_KEY) {
        digitalWrite(envelopeCV[keyboard], HIGH);
      } else if (digitalRead(newPitchSwitch[keyboard]) && lastKey[keyboard] != n ) {
        digitalWrite(envelopeCV[keyboard], LOW);
        retriggerRequested[keyboard] = true;
        retriggerTimer[keyboard] = millis();
      }
      lastKey[keyboard] = n;
      return;
    }
  }
  digitalWrite(envelopeCV[keyboard], LOW);
  retriggerRequested[keyboard] = false;
  lastKey[keyboard] = NO_KEY;
}


void midiNoteOn(byte channel, byte pitch, byte velocity) {
  if ( ( pitch < MIDI_KB_OFFSET ) || (pitch > MIDI_KB_OFFSET + MIDI_KB_KEYS)) {
    return;
  }
  if (velocity == 0) {
    processMidiNoteOff(channel, pitch, velocity);
    return;
  }
  for (int i = 0; i < 2; i++) {
    if (midiInChannel[i] == channel) {
      notes[i][pitch - MIDI_KB_OFFSET] = true;
      updateMIDIPitch(i);
      /* Next line is bug or a feature, not sure if original keyboard works like this.
         Setting velocity should maybe be done in updateMIDIPitch function. 
         Could be useful when recording events to the sequencer, that you can
         independently rewrite velocity value of event without touching
         pitch or key. */
      *velocityCV[i] = SEVEN_BIT_MAX - velocity * 2;
    }
  }
}


void midiNoteOff(byte channel, byte pitch, byte velocity) {
  if ( ( pitch < MIDI_KB_OFFSET ) || (pitch > MIDI_KB_OFFSET + MIDI_KB_KEYS)) {
    return;
  }
  processMidiNoteOff(channel, pitch, velocity);
}


void processMidiNoteOff(byte channel, byte pitch, byte velocity) {
  for (int i = 0; i < 2; i++) {
    if (midiInChannel[i] == channel) {
      notes[i][pitch - MIDI_KB_OFFSET] = false;
      updateMIDIPitch(i);
    }
  }
}


/* CC Interface. Controls from 20-25. 7-bit/1-bit data straight to outputs. */
void midiCC(byte channel, byte control, byte value) {
  if ( (channel == midiInChannel[0]) || (channel == midiInChannel[1])) {
    switch (control) {
      case 20: /* A */
        *pitchCV[0] = SEVEN_BIT_MAX - 2 * value;
        break;
      case 21: /* B */
        *velocityCV[0] = SEVEN_BIT_MAX - 2 * value;
        break;
      case 22: /* 1 */
        if (value > 63) {
          digitalWrite(envelopeCV[0], HIGH);
        } else {
          digitalWrite(envelopeCV[0], LOW);
        }
        break;
      case 23: /* C */
        *pitchCV[1] = SEVEN_BIT_MAX - 2 * value;
        break;
      case 24: /* D */
        *velocityCV[1] = SEVEN_BIT_MAX - 2 * value;
        break;
      case 25: /* 2 */
        if (value > 63) {
          digitalWrite(envelopeCV[1], HIGH);
        } else {
          digitalWrite(envelopeCV[1], LOW);
        }
        break;
      case 121:
      case 123:
        midiReset();
        break;
    }
  }
}


void midiReset() {
  for (int i = 0; i < 2; i++) {
    for (int n = MIDI_KB_KEYS; n > -1; n--) {
      notes[i][n] = false;
    }
    retriggerRequested[i] = false;
    digitalWrite(envelopeCV[i], LOW);
    *pitchCV[i] = SEVEN_BIT_MAX - 32 * 4;
    *velocityCV[i] = SEVEN_BIT_MAX;
  }
}
